import React, {Component} from 'react'
import {StyleSheet, View, Text, AsyncStorage, Button} from 'react-native'
import {TabNavigator, StackNavigator} from 'react-navigation'

import BillScreen from './bill'
import PlasticCardScreen from './plasticCard'
import LoginScreen from './login'
import DiscountScreen from './discount'
import ConfirmScreen from './confirm'
import ListMerchantScreen from './listMerchant'

let tokenKey = 'userTokenKey'

const Tabar = TabNavigator({
  Discount: {
    screen: DiscountScreen
  },
  Bill: {
    screen: BillScreen
  },
  Plastic: {
    screen: PlasticCardScreen
  }
})

const MainNavi = StackNavigator({
  Home: {
    screen: Tabar
  }
}, {
  navigationOptions: {
    // gesturesEnabled: false, headerTitle: 'Hello', headerRight = <Button
    // title={'Logout'} onPress={state.params.clickLogout ? state.params.clickLogout
    // : () => null}/>
  }
});

const AuthNav = StackNavigator({
  Login: {
    screen: LoginScreen
  },
  Confirm: {
    screen: ConfirmScreen
  },
  ListMerchant: {
    screen: ListMerchantScreen
  },
}, {
  headerMode: 'none'
}, {
  navigationOptions: {
    gesturesEnabled: true
  }
});

export default class PlashScreen extends Component {
  constructor(props) {
    super(props)
    this.state = ({isAuth: false, isGetConfig: false})
  }

  componentWillMount() {
    AsyncStorage.getItem(tokenKey, (error, result) => {
      let isAuth = false
      if (result) {
        isAuth = true
      }
      this.setState({isAuth: isAuth, isGetConfig: true})
    })
  }

  render() {
    if (this.state.isGetConfig) {
      if (this.state.isAuth) {
        return <MainNavi screenProps={{
          onLogout: this.onLogout
        }}/>
      }
      return <AuthNav screenProps={{
        onLogin: this.onLogin
      }}/>
    }
    return (<View style={styles.container}/>);
  }

  //---- On ----
  onLogin = (token) => {
    AsyncStorage.setItem(tokenKey, token)
    this.setState({isAuth: true})
  }

  onLogout = () => {
    AsyncStorage.removeItem(tokenKey)
    this.setState({isAuth: false})
  }
  //---- End On ----
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  }
});