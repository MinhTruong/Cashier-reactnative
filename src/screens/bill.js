import React, {Component} from 'react'
import {StackNavigator} from 'react-navigation'
import * as Cell from '../cells'
import styles from '../styles/styles'
import {
  StyleSheet,
  View,
  Text,
  Button,
  FlatList,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Image,
  AsyncStorage
} from 'react-native'

const itemType = {
  input: 'input',
  item: 'item',
  loading: 'loading',
  title:'title'
}

const input = {
  type: itemType.input,
  _id: (new Date()).getTime()
}

const loading = {
  type: itemType.loading,
  _id: (new Date()).getTime() + 1
}

export default class BillScreen extends Component {

  static navigationOptions = ({navigation}) => {
    const {
      params = {}
    } = navigation.state
    let headerRight = (<Button
      title={'Logout'}
      onPress={params.clickLogout
      ? params.clickLogout
      : () => null}/>)
    return {headerRight}
  }

  _clickLogout = () => {
    this
      .props
      .screenProps
      .onLogout()
  }

  constructor(props) {
    super(props)
    this.state = {
      token: '',
      mockData: [],
      total: 0,
      page: 0,
      isOnLoadmore: false,
    }
  }

  componentWillMount() {
    let data = this.state.mockData
    const totalTitle = {
      type: itemType.title,
      _id: (new Date()).getTime() + 1
    }
    data.push(input)
    data.push(totalTitle)
    this.setState({mockData: data})
    AsyncStorage.getItem('userTokenKey', (error, results) => {
      if (results) {
        this.setState({
          token: results
        })
        this.apiGetHistory(this.state.page)
      }
    })
  }

  componentDidMount() {
    this
      .props
      .navigation
      .setParams({clickLogout: this._clickLogout})
  }

  shouldComponentUpdate(nextPro, nextState) {
    if (nextState.mockData != this.state.mockData) {
      return true
    }
    return false
  }
  _keyExtractor = (item, index) => item._id
  _renderItem = (item) => {
    switch (item.item.type) {
      case itemType.input: return <Cell.BillInput/>
      case itemType.item: return <Cell.BillItem item={item.item}/>
      case itemType.loading: return <Cell.Loading/>
      case itemType.title: return <Cell.Title title={'LỊCH SỬ('+this.state.total+')'}/>
    }
  }
  _onEndReached = (distance) => {
    if (!this.state.isOnLoadmore || distance < 0) {
      return
    }
    const page = this.state.page + 1
    this.apiGetHistory(page)
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.mockData}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          onEndReachedThreshold={0.5}
          onEndReached={this._onEndReached}
          />
      </View>
    );
  }

  //---- Service ----
  apiGetHistory(page) {
    fetch('https://dev-api.zody.vn/bills?page=' + page, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.state.token
      }
    }).then((response) => response.json()).then((responseJson) => {
      if (responseJson.success) {
        let total = 0
        let isOnLoadmore = false
        let data = this.state.mockData.filter((item) => {
          return item.type != itemType.loading
        })
        if (responseJson.data.total) {
          total = responseJson.data.total
        }
        const bills = responseJson.data.bills
        if (bills && bills.length) {
          const temps = bills.map((item, index) => {
            item.type = itemType.item
            return item
          })
          data = data.concat(bills)
        }
        // Check loadmore
        let numOfItem = data.filter((item) => {
          return item.type === itemType.item
        }).length
        if (numOfItem < total) {
          data.push(loading)
          isOnLoadmore = true
        }
        // Set state
        this.setState({
          total: total,
          mockData: data,
          isOnLoadmore: isOnLoadmore,
          page: page
          })
      } else {
        let ms = responseJson.message
        Alert.alert('', ms)
      }
    }).catch((error) => {
      console.log('Request api login error: ==>', error)
    })
  }
}