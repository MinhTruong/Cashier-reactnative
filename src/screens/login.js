import React, {Component} from 'react'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import {Button} from '../components'
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TextInput,
  Image,
  Alert
} from 'react-native'

export default class LoginScreen extends Component {

  constructor(props) {
    super(props)
    this.state = {phoneNumber: '0905401173'}
  }

  render() {
    return (
      <KeyboardAwareScrollView style={styles.scrollView}>
        <View style={styles.container}>
          <View style={styles.contentView}>
            <Image
              style={styles.logo}
              resizeMode={'contain'}
              source={require('../resource/logo-zody.png')}/>
            <View>
              <Text style={styles.title}>Nhập số điện thoại để nhận mã xác nhận</Text>
            </View>
            <View style={styles.inputContent}>
              <TextInput
                style={styles.input}
                value={this.state.phoneNumber}
                placeholder='Số điện thoại'
                keyboardType='phone-pad'
                onChangeText = {(text) => this.onChangePhonenumber(text)}
                />
            </View>
            <Button title={'Nhận mã'} onPress={() => this.onclickLogin()} style={styles.touchContent}/>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }

  //---- Action ----
  onclickLogin = () => {
    this.apiLogin()
  }

  onChangePhonenumber = (phone) => {
    this.setState({phoneNumber: phone})
  }
  //---- End ----

  //---- Service ----
  apiLogin() {
    fetch('https://dev-api.zody.vn/login-cashier', {  
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        phone: this.state.phoneNumber
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.success) {
        let code = ''
        if (responseJson.data.code) {
          code = responseJson.data.code
        }
        const {navigate} = this.props.navigation
        navigate('Confirm', {phone:this.state.phoneNumber, code: code})
      } else {
        let ms = responseJson.message
        Alert.alert('', ms)
      }
    })
    .catch((error) => {
      console.log('Request api login error: ==>', error)
    })
  }
  //---- End Serview ----
}

//---- Styles ----
const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: '#F5FCFF'
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
    marginTop: 100
  },

  contentView: {
    flex: 1,
    width: 320,
    backgroundColor: '#FFFFFF'
  },

  logo: {
    width: 80,
    height: 60,
    marginTop: 10,
    alignSelf: 'center'
  },

  title: {
    marginLeft: 10,
    marginBottom: 10,
    marginTop: 10,
    fontSize: 14,
    fontStyle: 'italic'

  },

  inputContent: {
    height: 34,
    width: 300,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    justifyContent: 'center',
    alignSelf: 'center'
  },

  input: {
    marginLeft: 4,
    marginRight: 4
  },

  touchContent: {
    width: 300,
    marginTop: 20,
    alignSelf: 'center'
  }
});