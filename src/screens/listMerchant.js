import React, {Component} from 'react'
import {StyleSheet, View, Text, FlatList, TouchableOpacity, Alert} from 'react-native'

export default class PlasticCardScreen extends Component {

  constructor(props) {
    super(props)
    this.state = {
      selectedId: ''
    }
    this.params = this.props.navigation.state.params
  }
  _keyExtractor = (item, index) => item._id
  _renderItem = ({item}) => (
  <ListItem
    id={item._id}
    selected={false}
    merchant={item}
    onPressItem={this._onPressItem}
    isSelected={this.state.selectedId === item._id}
    />
  )
  _onPressItem = (id, token) => {
    this.setState({
      selectedId: id
    })
  }

  onpressChoose = () => {
    const merchants = this.params.merchants.filter((item) => {
      return item._id === this.state.selectedId
    })
    const token = merchants.length ? merchants[0].token : ''
    if (token) {
      this.props.screenProps.onLogin(token)
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.selectedId !== this.state.selectedId
  }


  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerView}>
            <Text style={styles.headerTitle}>Chọn cửa hàng</Text>
          </View>
        </View>
        <FlatList
          data={this.params.merchants}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          extraData={this.state}
          />
          <TouchableOpacity onPress={() => this.onpressChoose()}>
              <View style={styles.touchContent}>
                <Text style={styles.touchTitle}>Chọn</Text>
              </View>
            </TouchableOpacity>
      </View>
    )
  }
}

class ListItem extends Component {
  _onPress = () => {
    this.props.onPressItem(this.props.id, this.props.merchant.token)
  };

  render() {
    const bgColor = this.props.isSelected ? '#f60' : '#F5FCFF'
    const textColor = this.props.isSelected ? 'white' : 'black'
    return (
      <TouchableOpacity onPress={this._onPress}>
        <View style={[styles.itemContent, {backgroundColor: bgColor}]}>
          <Text numberOfLines={1} style={[styles.itemTextTitle, {color:textColor}]}>{this.props.merchant.name}</Text>
          <Text style={[styles.itemTextAddress, {color:textColor}]}>{this.props.merchant.address}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  header: {
    height: 64,
    width: '100%',
    backgroundColor: '#fff',
    alignItems: 'center'
  },
  headerView: {
    height: 44,
    marginTop: 20,
    justifyContent: 'center'
  },

  headerTitle: {
    fontSize: 17
  },
  touchContent: {
    height: 34,
    width: 300,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#f60',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },

  touchTitle: {
    color: '#fff',
    fontSize: 14
  },
  // Item
  itemContent: {
    marginLeft: 10,
    marginRight: 10,
    height: 70
  },

  itemTextTitle: {
    marginLeft: 16,
    marginRight: 16,
    marginTop: 10,
    fontSize: 17,
    fontWeight: 'bold'
  },

  itemTextAddress: {
    marginLeft: 16,
    marginRight: 16,
    marginTop: 2,
    fontSize: 14,
    fontStyle: 'italic'
  },
});