import SplashScreen from './splash'
import LoginScreen from './login'
import ConfirmScreen from './confirm'
import BillScreen from './bill'
import DiscountScreen from './discount'
import PlasticCardScreen from './plasticCard'
import ListMerChantScreen from './listMerchant'

export {
  SplashScreen,
  LoginScreen,
  ConfirmScreen,
  BillScreen,
  DiscountScreen,
  PlasticCardScreen,
  ListMerChantScreen
}