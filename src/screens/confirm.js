import React, {Component} from 'react'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import {Button} from '../components'
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native'

export default class LoginScreen extends Component {

  constructor(props) {
    super(props)
    this.state = {
      code: ''
    }
    this.params = this.props.navigation.state.params
  }

  componentDidMount() {
    if (this.params.code) {
      this.setState({
        code: this.params.code
      })
    }
  }

  render() {
    const {goBack} = this.props.navigation
    return (
      <KeyboardAwareScrollView style={styles.scrollView}>
        <View style={styles.container}>
          <View style={styles.contentView}>
            <Image
              style={styles.logo}
              resizeMode={'contain'}
              source={require('../resource/logo-zody.png')}/>
            <Text style={styles.title}>
              <Text>Mã xác nhận cho số điện thoại: </Text>
              <Text style={{color:'#f60', fontWeight:'bold'}}>{this.params.phone}</Text>
            </Text>
            <View style={styles.inputContent}>
              <TextInput
                style={styles.input}
                placeholder='123456'
                value={this.state.code}
                onChangeText={(text) => this.onCodeChange(text)}
                keyboardType='phone-pad'/>
            </View>
            <Button title={'Xác nhận'} onPress={() => this.onclickConfirm()} style={styles.touchContent}/>
            <TouchableOpacity onPress={() => goBack()}>
              <Text
                style={[
                styles.title, {
                  marginBottom: 30,
                  marginTop: 8,
                  color: '#999c9e'
                }
              ]}>Không nhận được mã xác nhận?</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }

  //---- Action ----
  onclickConfirm = () => {
    // this.props.screenProps.onLogin('Truong')
    this.apiConfirm()
  }

  onPop = () => {
    console.log('go back ==>')
    const {navigate} = this.props.navigation
    navigate.goBack('Login')
  }

  onCodeChange = (text) => {
    this.setState({
      code: text
    })
  }
  //---- End ----

   //---- Service ----
   apiConfirm() {
    fetch('https://dev-api.zody.vn/login-cashier', {  
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        phone: this.params.phone,
        code:this.state.code
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.success) {
        let data = responseJson.data
        if (data.merchants.length) {
          if (data.merchants.length == 1) {
            let token = data.merchants[0].token
            if (token) {
              this.props.screenProps.onLogin(token)
            }
          } else {
            const {navigate} = this.props.navigation
            navigate('ListMerchant', {merchants: data.merchants})
          }
          return
        }
      } 
      let ms = responseJson.message
      Alert.alert('', ms)
    })
    .catch((error) => {
      console.log('Request api login error: ==>', error)
    })
  }
  //---- End Serview ----
}

//---- Styles ----
const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: '#F5FCFF'
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
    marginTop: 100
  },

  contentView: {
    flex: 1,
    width: 320,
    backgroundColor: '#FFFFFF'
  },

  logo: {
    width: 80,
    height: 60,
    marginTop: 10,
    alignSelf: 'center'
  },

  title: {
    marginLeft: 10,
    marginBottom: 10,
    marginTop: 10,
    fontSize: 14,
    fontStyle: 'italic'

  },

  inputContent: {
    height: 34,
    width: 300,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    justifyContent: 'center',
    alignSelf: 'center'
  },

  input: {
    marginLeft: 4,
    marginRight: 4
  },

  touchContent: {
    width: 300,
    marginTop: 20,
    alignSelf: 'center'
  },

  touchTitle: {
    color: '#fff',
    fontSize: 14
  }
});