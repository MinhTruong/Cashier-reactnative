import React, {Component} from 'react'
import {
  View,
  Text,
  ActivityIndicator,
  StyleSheet
} from 'react-native'

export default class Loading extends Component {
  render() {
    return(
      <View style={styles.container}>
        <ActivityIndicator size='small' color='gray' />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center'
  }
})