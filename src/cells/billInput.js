import React, {Component} from 'react'
import {View, Text, TextInput, TouchableOpacity, StyleSheet} from 'react-native'
import {Button} from '../components'

export default class BillInput extends Component {

onPress = () => {
  console.log('====> click tich diem')
}

  render() {
    return (
      <View>
        <Text
          style={[
          styles.normaltext, {
            marginTop: 20
          }
        ]}>TÍCH ĐIỂM THÀNH VIÊN</Text>
        <View style={styles.inputContent}>
          <TextInput style={styles.input} placeholder='SĐT/ Mã thẻ/ Zody ID'/>
        </View>
        <Button title={'Tích điểm'} style={styles.touchContent} onPress={() => this.onPress()}/>
        <Text
          style={[
          styles.normaltext, {
            marginTop: 10
          }
        ]}>QUY ĐỊNH SỬ DỤNG</Text>
        <Text
          style={[
          styles.normaltext, {
            marginTop: 10
          }
        ]}>
          * Tích điểm ngay trong ngày cho khách bằng cách nhập SĐT/ Mã thẻ/ Zody ID bấm
          ENTER nhập số tiền và mã hóa đơn (nếu có). Nhân viên không được tự ý tích điểm,
          không tạo hóa đơn ảo, mọi vi phạm sẽ được thông báo cho chủ địa diểm. Không thể
          xóa bill sau khi tạo, nếu có bất kỳ sai xót nào vui lòng liên hệ với chủ/ quản
          lý để xóa bill đã tạo.
        </Text>
        <Text
          style={[
          styles.normaltext, {
            marginTop: 10,
            color: '#f60'
          }
        ]}>LIÊN HỆ HOTLINE: 0905 552 322</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#ECECEC'
  },
  inputContent: {
    height: 34,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    backgroundColor: '#fff',
    justifyContent: 'center'
  },

  input: {
    marginLeft: 4,
    marginRight: 4
  },

  touchContent: {
    marginTop: 16
  },

  touchTitle: {
    color: '#fff',
    fontSize: 14
  },

  normaltext: {
    fontSize: 15,
    marginLeft: 20,
    marginRight: 20
  },

  boldText: {
    fontSize: 14,
    fontWeight: 'bold'
  },

  blueText: {
    color: '#20a2ff'
  }
});
