import React, {Component} from 'react'
import {
  View,
  Text,
  StyleSheet
} from 'react-native'

export default class TitleItem extends Component {
  render() {
    return(
      <View style={styles.container}>
        <Text style={styles.text}>{this.props.title}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    alignItems: 'flex-start',
    justifyContent: 'flex-end'
  },

  text: {
    fontSize: 15,
    marginLeft: 10,
    marginRight: 10
  }
})