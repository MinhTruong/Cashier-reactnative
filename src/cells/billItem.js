import React, {Component} from 'react'
import moment from 'moment'
import {
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native'


export default class BillItem extends Component {
  render() {
    const {item} = this.props
    return(
      <View style={styles.itemContent}>
      <Text style={[styles.boldText, {color: '#f60'}]}>{item.user ? item.user.name : '--'}</Text>
      <View style={styles.itemContent3}>
      <View>
          <Text style={styles.boldText}>{item.plasticCard ? <Text>{item.plasticCard.serial}</Text> : <Text style={styles.blueText}>Chưa liên kết</Text>}</Text>
          <Text style={{marginTop: 4}}>{item.billId.length ? item.billId : '--'}</Text>
        </View>
        <View style={{alignItems: 'center', marginLeft: 10}}>
          <Text>{item.user ? item.user.phone : '--'}</Text>
          <Text style={[styles.blueText, {marginTop: 4}]}>{item.price}</Text>
        </View>
        <View style={{alignItems: 'flex-end'}}>
        <View style={{flexDirection: 'row'}}>
            <Text style={{marginRight: 4}}>{item.coin}</Text>
            <Image style={styles.zcoin} resizeMode={'contain'} source={require('../resource/zcoin.png')}/>
          </View>
          <Text style={[styles.blueText, {marginTop: 4}]}>{moment(item.createdAt).format('DD/MM/YYYY, hh:mm')}</Text>
        </View>
      </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  normaltext: {
    fontSize: 15,
    marginLeft: 20,
    marginRight: 20
  },

  boldText: {
    fontSize: 14,
    fontWeight: 'bold'
  },

  blueText: {
    color: '#20a2ff'
  },

  itemContent: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 5,
    padding: 8,
    backgroundColor: '#fff',
  },
  itemContent2: {
    marginTop: 4,
    flexDirection: 'row',
    justifyContent: 'space-between',
    display: 'flex'

  },
  itemContent3: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 4
  },
  zcoin: {
    height: 16,
    width: 16
  }
});