import React, {Component} from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native'

export default class Button extends Component {
  render() {
    return(
      <TouchableOpacity onPress={() => this.props.onPress()}>
      <View style={[styles.container, this.props.style]}>
        <Text style={styles.title}>{this.props.title}</Text>
      </View>
    </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: 34,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: '#f60',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center'
  },

  title: {
    color: '#fff',
    fontSize: 14
  },
})

