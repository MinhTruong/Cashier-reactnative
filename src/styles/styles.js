import React from 'react'
import {StyleSheet} from 'react-native'

export default StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#ECECEC'
  },
})