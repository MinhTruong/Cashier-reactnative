/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import * as Screen from './src/screens'


export default class App extends Component {
  render() {
    return (
      <Screen.SplashScreen/>
    );
  }
}
